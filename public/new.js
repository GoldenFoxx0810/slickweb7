$(document).ready(function() {
$('.responsive').slick({
  slidesToShow: 4,
  slidesToScroll: 4,
  arrows: true,
  infinite: true,
  dots: true,
  speed: 500,
  responsive: [
    {
      breakpoint: 600,
      settings: {
        adaptiveHeight:false,
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }
  ]
});
});